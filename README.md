# OpenML dataset: M4-competition-weekly

https://www.openml.org/d/46023

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

M4-Competition for time series forecasting, weekly data

From their website:
-----
The fourth competition, M4, started on 1 January 2018 and ended in 31 May 2018.

The M4 extended and replicated the results of the previous three competitions, using an extended and diverse set of time series to identify the most accurate forecasting method(s) for different types of predictions. It aimed to provide answers on how to improve forecasting accuracy and identify the most appropriate methods for each case. To get precise and compelling answers, the M4 Competition utilized 100,000 real-life series, and incorporated all major forecasting methods, including those based on Artificial Intelligence (Machine Learning, ML), as well as traditional statistical ones.
-----

The time series were downloaded via the R package M4comp2018 and then loaded with a python script to obtain the different datasets:
'Yearly', 'Quarterly', 'Monthly', 'Weekly', 'Daily', 'Hourly'. The data in R already gives us some kind of date as the index for the time series.

There are 5 columns:

id_series: The identifier of a time series.

Category: The category of a time series.

Value: The value of the time series at 'time_step'.

date: The reconstructed date of the time series in the format %Y-%m-%d.

Note that the participants did not have access to the date of the time series during the competition. Besides, some dates are ambiguous due to 
the representation of only 2 digits for the year (XX-XX-17 could represent 1817, 1917, 2017 etc).

Preprocessing:

1 - The original index were the number of weeks since the unix epoch (01-01-1970), we transformed it in the 'date'.

This time we did not correct for year > 2018, even though we identified year > 2018 for seris 'W44', 'W40', 'W48', 'W39'.

2 - Defined columns 'id_series' and 'Category' as 'category', casted 'date' to str and 'time_step' to int.

The required number of forecast values to be forecasted for each time series, for the 'Weekly' dataset was always 13. Therefore, if one wants to evaluate 
their model to be compared with other models from the original competition, the last 13 values of each time series are considered the test dataset.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46023) of an [OpenML dataset](https://www.openml.org/d/46023). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46023/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46023/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46023/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

